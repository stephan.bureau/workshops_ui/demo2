import * as PIXI from "pixi.js";
import { TweenMax, PixiPlugin } from "gsap/all";
const gsapPlugins = [TweenMax, PixiPlugin];
PixiPlugin.registerPIXI(PIXI);
import { createSpriteMenu } from "./components/menu";
import { createSpriteLifes } from "./components/lifes";
import { Game } from "./components/game";

class MyGame {
  constructor() {
    // crée le moteur de jeu
    this.app = new PIXI.Application({
      width: 800,
      height: 600
    });
    document.body.appendChild(this.app.view);

    // définit l'état de jeu au démarrage
    this.state = {
      current: "niveau1",
      isMenuOpened: false
    };

    this.init();
  }

  /**
   * Initialize le jeu
   */
  async init() {
    // créé le plateau de jeu
    this.game = new Game();
    this.gameSprite = await this.game.createSpriteGame({ app: this.app });
    this.app.stage.addChild(this.gameSprite);

    // créé les vies
    this.lifes = await createSpriteLifes({ app: this.app });
    this.app.stage.addChild(this.lifes);

    // créé les menus
    this.menu = createSpriteMenu();
    this.app.stage.addChild(this.menu);

    // affiche le niveau 1 et le menu
    this.manageMenuVisibility(false, true);

    // ajoute les écouteurs d'évènements
    this.addListeners();

    // boucle de jeu
    this.app.ticker.add(() => {
      this.game.ticker();
    });
  }

  // ajoute les écouteurs d'évènements
  addListeners() {
    // évènement de clavier
    document.addEventListener("keyup", event => {
      if (event.defaultPrevented) return;
      switch (event.key) {
        case "Escape":
          this.manageMenuVisibility();
          break;
        case " ":
          this.game.goblin.jump();
          break;
      }
    });

    // événement de jeu
    document.addEventListener("clickMenu", event => {
      if (this.state.isMenuOpened) {
        const buttonName = event.detail.buttonName;
        switch (buttonName) {
          case "exit":
            this.manageMenuVisibility(false);
            break;
          default:
        }
      }
    });
  }

  // affiche / cache le menu principal
  manageMenuVisibility(isDisplayed = true, instant = false) {
    if (instant) {
      this.menu.visible = isDisplayed;
    } else {
      // anime l'effet de transparence de 0 à 1, et vice-versa
      TweenMax.to(this.menu, 0.25, {
        pixi: {
          alpha: isDisplayed ? 1 : 0,
          onComplete: () => {
            this.menu.visible = isDisplayed;
          }
        }
      });
    }
    this.state = {
      ...this.state,
      isMenuOpened: isDisplayed
    };
  }
}

const myGame = new MyGame();


// ajout d'information sur le jeu
const textContainer = document.createElement('div');
textContainer.style.backgroundColor = "#ffffff";
textContainer.style.position = "absolute";
textContainer.style.top = 0;
textContainer.style.left = 0;

const infoText = document.createElement('h2');
infoText.innerHTML = "Bienvenue dans ma démo du jeudi !";
textContainer.style.color = "#000000";

textContainer.appendChild(infoText);

document.body.appendChild(textContainer);
import * as PIXI from "pixi.js";
import BG1 from "../assets/Layers/Sky.png";
import BG2 from "../assets/Layers/BG_Decor.png";
import BG3 from "../assets/Layers/Middle_Decor.png";
import BG4 from "../assets/Layers/Foreground.png";
import BG5 from "../assets/Layers/Ground.png";
import { create } from "domain";
import { Goblin } from "./goblin";

// import { BloomFilter } from "@pixi/filter-bloom";
// import { ZoomBlurFilter } from "@pixi/filter-zoom-blur";
// import { AsciiFilter } from "@pixi/filter-ascii";
// import { CrossHatchFilter } from "@pixi/filter-cross-hatch";

export class Game {
  ticker() {
    const multiplier = 40;
    if (this.bg1) this.bg1.tilePosition.x -= multiplier * 0.12;
    if (this.bg2) this.bg2.tilePosition.x -= multiplier * 0.16;
    if (this.bg3) this.bg3.tilePosition.x -= multiplier * 0.2;
    if (this.bg4) this.bg4.tilePosition.x -= multiplier * 0.26;
    if (this.bg5) this.bg5.tilePosition.x -= multiplier * 0.4;
    if (this.goblin) this.goblin.ticker();
  }

  // créé le background avec la texture désirée
  createBg(texture, container) {
    const tilingSprite = new PIXI.TilingSprite(
      texture,
      texture.baseTexture.width,
      texture.baseTexture.height
    );
    tilingSprite.scale.set(0.5, 0.56);
    tilingSprite.tilePosition.x = 0;
    tilingSprite.tilePosition.y = 0;

    // tilingSprite.filters = [
    //   new BloomFilter(),
    //   new CrossHatchFilter()
    //   // new AsciiFilter(6)
    // ];

    container.addChild(tilingSprite);
    return tilingSprite;
  }

  // créé le plateau de jeu et ses éléments graphiques
  createSpriteGame({ app }) {
    return new Promise((resolve, reject) => {
      this.gameParralax = new PIXI.Container();
      app.loader
        .add("bg1", BG1)
        .add("bg2", BG2)
        .add("bg3", BG3)
        .add("bg4", BG4)
        .add("bg5", BG5)
        .load(async (_, resources) => {
          this.bg1 = this.createBg(resources.bg1.texture, app.stage);
          this.bg2 = this.createBg(resources.bg2.texture, app.stage);
          this.bg3 = this.createBg(resources.bg3.texture, app.stage);
          this.bg4 = this.createBg(resources.bg4.texture, app.stage);
          this.bg5 = this.createBg(resources.bg5.texture, app.stage);

          // crée le goblin et l'ajoute au plateau de jeu
          this.goblin = new Goblin();
          const goblinSprite = await this.goblin.createSpriteGoblin({ app });
          goblinSprite.scale.set(0.5);
          goblinSprite.x = 300;
          goblinSprite.y = app.screen.height + 10;
          this.bg4.addChild(goblinSprite);

          resolve(this.gameParralax);
        });
    });
  }
}

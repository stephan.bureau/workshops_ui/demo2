import * as PIXI from "pixi.js";
import { TweenMax, PixiPlugin } from "gsap/all";

// importation des textures de saut
import Jump0 from "../assets/goblin/Jump_Loop/0_Goblin_Jump_Loop_000.png";
import Jump1 from "../assets/goblin/Jump_Loop/0_Goblin_Jump_Loop_001.png";
import Jump2 from "../assets/goblin/Jump_Loop/0_Goblin_Jump_Loop_002.png";
import Jump3 from "../assets/goblin/Jump_Loop/0_Goblin_Jump_Loop_003.png";
import Jump4 from "../assets/goblin/Jump_Loop/0_Goblin_Jump_Loop_004.png";
import Jump5 from "../assets/goblin/Jump_Loop/0_Goblin_Jump_Loop_005.png";

// important des textures de course
import Running0 from "../assets/goblin/Running/0_Goblin_Running_000.png";
import Running1 from "../assets/goblin/Running/0_Goblin_Running_001.png";
import Running2 from "../assets/goblin/Running/0_Goblin_Running_002.png";
import Running3 from "../assets/goblin/Running/0_Goblin_Running_003.png";
import Running4 from "../assets/goblin/Running/0_Goblin_Running_004.png";
import Running5 from "../assets/goblin/Running/0_Goblin_Running_005.png";
import Running6 from "../assets/goblin/Running/0_Goblin_Running_006.png";
import Running7 from "../assets/goblin/Running/0_Goblin_Running_007.png";
import Running8 from "../assets/goblin/Running/0_Goblin_Running_008.png";
import Running9 from "../assets/goblin/Running/0_Goblin_Running_009.png";
import Running10 from "../assets/goblin/Running/0_Goblin_Running_010.png";
import Running11 from "../assets/goblin/Running/0_Goblin_Running_011.png";

import { create } from "domain";

// import { PixelateFilter } from "@pixi/filter-pixelate";

export class Goblin {
  ticker() {}
  // déclenche un saut du goblin
  jump() {
    // si l'animation de saut du goblin n'est pas terminée
    if (this.jumpingGoblin.visible) return;

    // cacher l'animation de course du goblin
    this.runningGoblin.visible = false;

    // déclencher l'animation de saut du goblin
    this.jumpingGoblin.gotoAndPlay(0);

    // affiche le goblin qui saute
    this.jumpingGoblin.visible = true;

    // anime la distance de saut du goblin
    TweenMax.to(this.jumpingGoblin, 0.32, {
      pixi: {
        y: "-=400px"
      },
      repeat: 1,
      yoyo: true,
      // se déclencher lorsque le goblin a terminé son saut
      onComplete: () => {
        // affiche le goblin qui court et cache le goblin qui saute
        this.runningGoblin.visible = true;
        this.jumpingGoblin.visible = false;
      }
    });
  }

  // créé le goblin et ses élements graphiques
  createSpriteGoblin({ app }) {
    return new Promise(resolve => {
      this.goblinContainer = new PIXI.Container();
      const runningGoblinTextures = [];
      const jumpingGoblinTextures = [];

      app.loader
        // charge en mémoire les textures de saut
        .add("jumping0", Jump0)
        .add("jumping1", Jump1)
        .add("jumping2", Jump2)
        .add("jumping3", Jump3)
        .add("jumping4", Jump4)
        .add("jumping5", Jump5)
        // charge en mémoire les textures de course
        .add("running0", Running0)
        .add("running1", Running1)
        .add("running2", Running2)
        .add("running3", Running3)
        .add("running4", Running4)
        .add("running5", Running5)
        .add("running6", Running6)
        .add("running7", Running7)
        .add("running8", Running8)
        .add("running9", Running9)
        .add("running10", Running10)
        .add("running11", Running11)
        .load((_, resources) => {
          // réunit les textures en animation de course
          for (let i = 0; i < 12; i++) {
            const texture = new PIXI.Texture(resources[`running${i}`].texture);
            runningGoblinTextures.push(texture);
          }
          // créé le goblin animé qui court et l'ajoute au container du goblin et joue l'animation
          this.runningGoblin = new PIXI.AnimatedSprite(runningGoblinTextures);
          this.runningGoblin.animationSpeed = 0.3;
          this.runningGoblin.play();
          // this.runningGoblin.filters = [new PixelateFilter()];
          this.goblinContainer.addChild(this.runningGoblin);

          // réunit les textures en animation de saut
          for (let i = 0; i < 6; i++) {
            const texture = new PIXI.Texture(resources[`jumping${i}`].texture);
            jumpingGoblinTextures.push(texture);
          }

          // créé le goblin animé qui saute et l'ajoute au container du goblin
          this.jumpingGoblin = new PIXI.AnimatedSprite(jumpingGoblinTextures);
          this.jumpingGoblin.loop = false;
          this.jumpingGoblin.visible = false;
          this.jumpingGoblin.animationSpeed = 0.5;
          // this.jumpingGoblin.filters = [new PixelateFilter()];
          this.goblinContainer.addChild(this.jumpingGoblin);

          resolve(this.goblinContainer);
        });
    });
  }
}

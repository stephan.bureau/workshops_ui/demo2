import * as PIXI from "pixi.js";
import hearthUrl from "../assets/heart.png";
import { create } from "domain";

let lifes = [{}, {}, {}, {}],
  lifesContainer;

export const removeLife = () => {
  lifes.pop();
};

export const addLife = () => {
  lifes.push(createLifeSprite(lifes.length + 1));
};

const createLifeSprite = (resources, key) => {
  const life = new PIXI.Sprite(resources.heartUrl.texture);
  life.anchor.set(0.5);
  life.scale.set(0.5);
  life.x += key * 50;
  lifesContainer.addChild(life);
  return life;
};

export const createSpriteLifes = ({ app }) => {
  return new Promise(resolve => {
    lifesContainer = new PIXI.Container();
    lifesContainer.x = app.screen.width - 40 * 4 - 40;
    lifesContainer.y = 60;

    app.loader
      .add("heartUrl", hearthUrl, { parentResource: app.loader.resources })
      .load((_, resources) => {
        lifes.map((__, key) => createLifeSprite(resources, key));
        resolve(lifesContainer);
      });
  });
};

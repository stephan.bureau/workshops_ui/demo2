import * as PIXI from "pixi.js";

let exampleText, niveau2Text, exitText;

const onPointerOver = event => {
  event.currentTarget.style = styleHover;
};
const onPointerOut = event => {
  event.currentTarget.style = style;
};

const onPointerDown = event => {
  switch (event.currentTarget) {
    case exampleText:
      document.dispatchEvent(
        new CustomEvent("clickMenu", { detail: { buttonName: "example" } })
      );
      break;
    case exitText:
      document.dispatchEvent(
        new CustomEvent("clickMenu", { detail: { buttonName: "exit" } })
      );
      break;
  }
};

const style = new PIXI.TextStyle({
  fontFamily: "Arial",
  fontSize: 36,
  // fontStyle: "italic",
  fontWeight: "bold",
  fill: ["#ffffff", "#fc0"], // gradient
  stroke: "#000",
  strokeThickness: 4,
  dropShadow: true,
  dropShadowColor: "#000000",
  dropShadowBlur: 6,
  dropShadowAngle: Math.PI / 6,
  dropShadowDistance: 3,
  wordWrap: true,
  wordWrapWidth: 440
});

const styleHover = { ...style, fill: ["magenta"] };

const addInteractions = button => {
  button.interactive = true;
  button.buttonMode = true;
  button
    .on("pointerdown", onPointerDown)
    // .on("pointerup", onPointerUp)
    // .on("pointerupoutside", onButtonUp)
    .on("pointerover", onPointerOver)
    .on("pointerout", onPointerOut);
};

export const createSpriteMenu = () => {
  const menu = new PIXI.Container();

  const background = new PIXI.Graphics();
  background.alpha = 0.7;
  background.beginFill(0xffffff);
  background.drawRect(0, 0, 800, 600);
  background.endFill();
  menu.addChild(background);

  const menuContainer = new PIXI.Graphics();
  menuContainer.alpha = 1;
  menuContainer.beginFill(0xffffff);
  menuContainer.drawRect(300, 150, 200, 300);
  menuContainer.endFill();
  menu.addChild(menuContainer);

  exampleText = new PIXI.Text("Example", style);
  exampleText.x = 325;
  exampleText.y = 160;
  addInteractions(exampleText);
  menu.addChild(exampleText);

  exitText = new PIXI.Text("Exit", style);
  exitText.x = 360;
  exitText.y = 380;
  addInteractions(exitText);
  menu.addChild(exitText);

  return menu;
};

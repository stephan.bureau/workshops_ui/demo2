const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const distFolder = path.resolve(`${__dirname}/dist`);

module.exports = {
  entry: "./src/index.js",

  output: {
    path: distFolder,
    filename: "index_bundle.js"
  },

  module: {
    rules: [
      {
        test: /\.(png|svg|jpg|gif)$/,
        use: ["file-loader"]
      }
    ]
  },

  plugins: [new HtmlWebpackPlugin()]
};
